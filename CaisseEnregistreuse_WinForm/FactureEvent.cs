﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaisseEnregistreuse_WinForm
{
    public class FactureEvent
    {
        public FactureEventType Type { get; set; }
        public LigneFacture LigneFacture { get; set; }
        public Facture Facture { get; set; }
    }
}
