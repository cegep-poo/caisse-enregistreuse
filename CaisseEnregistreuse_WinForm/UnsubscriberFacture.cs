﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaisseEnregistreuse_WinForm
{
    public class UnsubscriberFacture : IDisposable
    {
        private IObserver<FactureEvent> m_observateur;
        private List<IObserver<FactureEvent>> m_observateurs;
        public UnsubscriberFacture(List<IObserver<FactureEvent>> p_observateurs, IObserver<FactureEvent> p_observateur)
        {
            if (p_observateur == null)
            {
                throw new ArgumentNullException(nameof(p_observateur));
            }
            if (p_observateurs == null)
            {
                throw new ArgumentNullException(nameof(p_observateurs));
            }
            this.m_observateurs = p_observateurs;
            this.m_observateur = p_observateur;
        }
        public void Dispose()
        {
            this.m_observateurs.Remove(this.m_observateur);
        }
    }
}
