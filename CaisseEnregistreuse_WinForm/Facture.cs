﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaisseEnregistreuse_WinForm
{
    public class Facture : IObservable<FactureEvent>
    {
        private List<IObserver<FactureEvent>> m_observateurs;
        private List<LigneFacture> m_LignesFacture;
        public decimal Total { get; private set; }
        public Facture()
        {
            this.m_observateurs = new List<IObserver<FactureEvent>>();
            this.m_LignesFacture = new List<LigneFacture>();
            this.CalculerTotal();
        }
        private void CalculerTotal()
        {
            decimal total = 0.00m;

            foreach (LigneFacture ligneFacture in this.m_LignesFacture)
            {
                total += ligneFacture.Total;
            }

            this.Total = total;
        }
        public int ObtenirNombreLignesFacture()
        {
            if(this.m_LignesFacture == null)
            {
                throw new ArgumentNullException(nameof(this.m_LignesFacture));
            }

            return this.m_LignesFacture.Count;
        }
        public void AjouterLigneFacture(LigneFacture p_ligneFacture)
        {
            if(p_ligneFacture == null)
            {
                throw new ArgumentNullException(nameof(p_ligneFacture));
            }
            this.m_LignesFacture.Add(p_ligneFacture);
            this.CalculerTotal();
            FactureEvent nouvelEvenement = new FactureEvent() { Type = FactureEventType.AJOUT_LIGNE, Facture = this, LigneFacture = p_ligneFacture };
            this.NotifierObservateurs(nouvelEvenement);
        }
        public void SupprimerLignesFacture()
        {
            this.CalculerTotal();
            FactureEvent nouvelEvenement = new FactureEvent() { Type = FactureEventType.NOUVELLE, Facture = this, LigneFacture = null };
            this.NotifierObservateurs(nouvelEvenement);
            this.m_LignesFacture = null;
            this.m_LignesFacture = new List<LigneFacture>();
        }
        public IDisposable Subscribe(IObserver<FactureEvent> p_observateur)
        {
            if(p_observateur == null)
            {
                throw new ArgumentNullException(nameof(p_observateur));
            }

            this.m_observateurs.Add(p_observateur);
            return new UnsubscriberFacture(this.m_observateurs, p_observateur);
        }
        private void NotifierObservateurs(FactureEvent p_evenement)
        {
            this.m_observateurs.ForEach(observateur => observateur.OnNext(p_evenement));
        }
    }
}
