﻿
namespace CaisseEnregistreuse_WinForm
{
    partial class fPrincipale
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvLignesFacture = new System.Windows.Forms.DataGridView();
            this.cDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cQuantite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPrixUnitaire = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbArticle = new System.Windows.Forms.Label();
            this.gbSimulateurScanner = new System.Windows.Forms.GroupBox();
            this.bSimuler = new System.Windows.Forms.Button();
            this.lbPrix = new System.Windows.Forms.Label();
            this.lbQuantite = new System.Windows.Forms.Label();
            this.nudPrix = new System.Windows.Forms.NumericUpDown();
            this.nudQuantite = new System.Windows.Forms.NumericUpDown();
            this.lbDescription = new System.Windows.Forms.Label();
            this.tbArticle = new System.Windows.Forms.TextBox();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.lbTotal = new System.Windows.Forms.Label();
            this.bPayer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLignesFacture)).BeginInit();
            this.gbSimulateurScanner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantite)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvLignesFacture
            // 
            this.dgvLignesFacture.AllowUserToAddRows = false;
            this.dgvLignesFacture.AllowUserToDeleteRows = false;
            this.dgvLignesFacture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvLignesFacture.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvLignesFacture.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLignesFacture.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cDescription,
            this.cQuantite,
            this.cPrixUnitaire,
            this.cTotal});
            this.dgvLignesFacture.Location = new System.Drawing.Point(36, 91);
            this.dgvLignesFacture.Name = "dgvLignesFacture";
            this.dgvLignesFacture.ReadOnly = true;
            this.dgvLignesFacture.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvLignesFacture.RowHeadersWidth = 123;
            this.dgvLignesFacture.RowTemplate.Height = 57;
            this.dgvLignesFacture.Size = new System.Drawing.Size(2481, 721);
            this.dgvLignesFacture.TabIndex = 0;
            // 
            // cDescription
            // 
            this.cDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cDescription.HeaderText = "Description";
            this.cDescription.MinimumWidth = 15;
            this.cDescription.Name = "cDescription";
            this.cDescription.ReadOnly = true;
            // 
            // cQuantite
            // 
            this.cQuantite.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cQuantite.HeaderText = "Quantité";
            this.cQuantite.MinimumWidth = 15;
            this.cQuantite.Name = "cQuantite";
            this.cQuantite.ReadOnly = true;
            // 
            // cPrixUnitaire
            // 
            this.cPrixUnitaire.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cPrixUnitaire.HeaderText = "Prix Unitaire";
            this.cPrixUnitaire.MinimumWidth = 15;
            this.cPrixUnitaire.Name = "cPrixUnitaire";
            this.cPrixUnitaire.ReadOnly = true;
            // 
            // cTotal
            // 
            this.cTotal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cTotal.HeaderText = "Total";
            this.cTotal.MinimumWidth = 15;
            this.cTotal.Name = "cTotal";
            this.cTotal.ReadOnly = true;
            // 
            // lbArticle
            // 
            this.lbArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbArticle.AutoSize = true;
            this.lbArticle.Location = new System.Drawing.Point(36, 13);
            this.lbArticle.Name = "lbArticle";
            this.lbArticle.Size = new System.Drawing.Size(155, 48);
            this.lbArticle.TabIndex = 1;
            this.lbArticle.Text = "Articles :";
            // 
            // gbSimulateurScanner
            // 
            this.gbSimulateurScanner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSimulateurScanner.Controls.Add(this.bSimuler);
            this.gbSimulateurScanner.Controls.Add(this.lbPrix);
            this.gbSimulateurScanner.Controls.Add(this.lbQuantite);
            this.gbSimulateurScanner.Controls.Add(this.nudPrix);
            this.gbSimulateurScanner.Controls.Add(this.nudQuantite);
            this.gbSimulateurScanner.Controls.Add(this.lbDescription);
            this.gbSimulateurScanner.Controls.Add(this.tbArticle);
            this.gbSimulateurScanner.Location = new System.Drawing.Point(36, 1018);
            this.gbSimulateurScanner.Name = "gbSimulateurScanner";
            this.gbSimulateurScanner.Size = new System.Drawing.Size(2481, 288);
            this.gbSimulateurScanner.TabIndex = 2;
            this.gbSimulateurScanner.TabStop = false;
            this.gbSimulateurScanner.Text = "Simulateur scanner";
            // 
            // bSimuler
            // 
            this.bSimuler.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bSimuler.Location = new System.Drawing.Point(2204, 170);
            this.bSimuler.Name = "bSimuler";
            this.bSimuler.Size = new System.Drawing.Size(242, 69);
            this.bSimuler.TabIndex = 6;
            this.bSimuler.Text = "Simuler";
            this.bSimuler.UseVisualStyleBackColor = true;
            this.bSimuler.Click += new System.EventHandler(this.bSimuler_Click);
            // 
            // lbPrix
            // 
            this.lbPrix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPrix.AutoSize = true;
            this.lbPrix.Location = new System.Drawing.Point(2070, 70);
            this.lbPrix.Name = "lbPrix";
            this.lbPrix.Size = new System.Drawing.Size(79, 48);
            this.lbPrix.TabIndex = 5;
            this.lbPrix.Text = "Prix";
            // 
            // lbQuantite
            // 
            this.lbQuantite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbQuantite.AutoSize = true;
            this.lbQuantite.Location = new System.Drawing.Point(1488, 70);
            this.lbQuantite.Name = "lbQuantite";
            this.lbQuantite.Size = new System.Drawing.Size(157, 48);
            this.lbQuantite.TabIndex = 4;
            this.lbQuantite.Text = "Quantité";
            // 
            // nudPrix
            // 
            this.nudPrix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nudPrix.DecimalPlaces = 2;
            this.nudPrix.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudPrix.Location = new System.Drawing.Point(2204, 65);
            this.nudPrix.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudPrix.Name = "nudPrix";
            this.nudPrix.Size = new System.Drawing.Size(242, 55);
            this.nudPrix.TabIndex = 3;
            // 
            // nudQuantite
            // 
            this.nudQuantite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nudQuantite.Location = new System.Drawing.Point(1680, 68);
            this.nudQuantite.Name = "nudQuantite";
            this.nudQuantite.Size = new System.Drawing.Size(254, 55);
            this.nudQuantite.TabIndex = 2;
            // 
            // lbDescription
            // 
            this.lbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDescription.AutoSize = true;
            this.lbDescription.Location = new System.Drawing.Point(22, 67);
            this.lbDescription.Name = "lbDescription";
            this.lbDescription.Size = new System.Drawing.Size(201, 48);
            this.lbDescription.TabIndex = 1;
            this.lbDescription.Text = "Description";
            // 
            // tbArticle
            // 
            this.tbArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbArticle.Location = new System.Drawing.Point(253, 67);
            this.tbArticle.Name = "tbArticle";
            this.tbArticle.Size = new System.Drawing.Size(1094, 55);
            this.tbArticle.TabIndex = 0;
            // 
            // tbTotal
            // 
            this.tbTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotal.Location = new System.Drawing.Point(2106, 847);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.ReadOnly = true;
            this.tbTotal.Size = new System.Drawing.Size(410, 55);
            this.tbTotal.TabIndex = 3;
            // 
            // lbTotal
            // 
            this.lbTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotal.AutoSize = true;
            this.lbTotal.Location = new System.Drawing.Point(1948, 850);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(95, 48);
            this.lbTotal.TabIndex = 5;
            this.lbTotal.Text = "Total";
            // 
            // bPayer
            // 
            this.bPayer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bPayer.Location = new System.Drawing.Point(2274, 932);
            this.bPayer.Name = "bPayer";
            this.bPayer.Size = new System.Drawing.Size(242, 69);
            this.bPayer.TabIndex = 7;
            this.bPayer.Text = "Payer";
            this.bPayer.UseVisualStyleBackColor = true;
            this.bPayer.Click += new System.EventHandler(this.bPayer_Click);
            // 
            // fPrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(20F, 48F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2548, 1341);
            this.Controls.Add(this.bPayer);
            this.Controls.Add(this.lbTotal);
            this.Controls.Add(this.tbTotal);
            this.Controls.Add(this.gbSimulateurScanner);
            this.Controls.Add(this.lbArticle);
            this.Controls.Add(this.dgvLignesFacture);
            this.MinimumSize = new System.Drawing.Size(2500, 1400);
            this.Name = "fPrincipale";
            this.Text = "Écran principal";
            ((System.ComponentModel.ISupportInitialize)(this.dgvLignesFacture)).EndInit();
            this.gbSimulateurScanner.ResumeLayout(false);
            this.gbSimulateurScanner.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLignesFacture;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn cQuantite;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPrixUnitaire;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTotal;
        private System.Windows.Forms.Label lbArticle;
        private System.Windows.Forms.GroupBox gbSimulateurScanner;
        private System.Windows.Forms.Button bSimuler;
        private System.Windows.Forms.Label lbPrix;
        private System.Windows.Forms.Label lbQuantite;
        private System.Windows.Forms.NumericUpDown nudPrix;
        private System.Windows.Forms.NumericUpDown nudQuantite;
        private System.Windows.Forms.Label lbDescription;
        private System.Windows.Forms.TextBox tbArticle;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.Label lbTotal;
        private System.Windows.Forms.Button bPayer;
    }
}

