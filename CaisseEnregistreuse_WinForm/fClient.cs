﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CaisseEnregistreuse_WinForm
{
    public partial class fClient : Form
    {
        private Facture m_facture;
        public fClient(Facture p_facture)
        {
            if (p_facture == null)
            {
                throw new ArgumentNullException(nameof(p_facture));
            }

            this.m_facture = p_facture;

            Action<FactureEvent> reagir = evenement =>
            {
                string texteClient;

                if (evenement.Type.Equals(FactureEventType.AJOUT_LIGNE))
                {
                    string description = evenement.LigneFacture.Description;
                    string quantite = evenement.LigneFacture.Quantite.ToString();
                    string prix = evenement.LigneFacture.PrixUnitaire.ToString() + " $";

                    texteClient = description.Substring(0, Math.Min(description.Length, 10)) + "  " + quantite + " @ " + prix;
                }
                else
                {
                    texteClient = "Bienvenue";
                }
                this.tbArticle.Text = texteClient;
            };

            ObservateurFacture observateurFacture = new ObservateurFacture(this.m_facture, reagir);

            InitializeComponent();
        }
    }
}
