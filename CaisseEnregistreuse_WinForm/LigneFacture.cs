﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaisseEnregistreuse_WinForm
{
    public class LigneFacture
    {
        public string Description { get; private set; }
        public int Quantite { get; private set; }
        public decimal PrixUnitaire { get; private set; }
        public decimal Total { get; private set; }
        public LigneFacture(string p_description, int p_quantite, decimal p_prix)
        {
            if(p_description == null)
            {
                throw new ArgumentNullException(nameof(p_description));
            }
            if(p_quantite < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(p_quantite));
            }
            if(p_prix < 0.01m)
            {
                throw new ArgumentOutOfRangeException(nameof(p_prix));
            }

            this.Description = p_description;
            this.Quantite = p_quantite;
            this.PrixUnitaire = p_prix;
            CalculerTotal();
        }
        private void CalculerTotal()
        {
            this.Total = this.Quantite * this.PrixUnitaire;
        }
    }
}
