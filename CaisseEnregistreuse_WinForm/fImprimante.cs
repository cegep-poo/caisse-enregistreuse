﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CaisseEnregistreuse_WinForm
{
    public partial class fImprimante : Form
    {
        private Facture m_facture;
        private DateTime m_DateAujourdhui;
        public fImprimante(Facture p_facture)
        {
            if(p_facture == null)
            {
                throw new ArgumentNullException(nameof(p_facture));
            }

            this.m_facture = p_facture;
            this.m_DateAujourdhui = DateTime.Now;

            Action<FactureEvent> ImprimerFacture = evenement =>
            {
                if (evenement.Type.Equals(FactureEventType.AJOUT_LIGNE))
                {
                    if(this.m_facture.ObtenirNombreLignesFacture() == 1)
                    {
                        this.tbFacture.Text = "Bienvenue à PFL-Mart\r\n\r\n2410 Ch Sainte-Foy\r\nQuebec City\r\nQC, G1V 1T3\r\nCanada\r\n\r\n" + this.m_DateAujourdhui.ToString() +"\r\n\r\n\r\n";

                        this.tbFacture.Text += "Description   |   Quantité   |   Prix unitaire   |   Total\r\n";
                        this.tbFacture.Text += "--------------------------------------------------------------------\r\n";
                    }

                    string description = evenement.LigneFacture.Description;
                    description = description.Substring(0, Math.Min(description.Length, 10));
                    string quantite = evenement.LigneFacture.Quantite.ToString();
                    string prix = evenement.LigneFacture.PrixUnitaire.ToString() + " $";
                    string total = evenement.LigneFacture.Total.ToString() + " $";

                    int paddingDescription = 20 - description.Length;
                    int paddingQuantite = 10 - quantite.Length;
                    int paddingPrix = 20 - prix.Length;
                    int paddingTotal = 15 - total.Length;

                    description = AlignerGauche(paddingDescription, description);
                    quantite = AlignerDroite(paddingQuantite, quantite);
                    prix = AlignerDroite(paddingPrix, prix);
                    total = AlignerDroite(paddingTotal, total);

                    this.tbFacture.Text += description + quantite + prix + total + "\r\n";
                }

                if(evenement.Type.Equals(FactureEventType.NOUVELLE) && this.m_facture.ObtenirNombreLignesFacture() > 0)
                {
                    this.tbFacture.Text += "--------------------------------------------------------------------\r\n";

                    string totalFacture = "Total à payer: " + this.m_facture.Total.ToString() + " $";
                    int paddingTotal = 50 - totalFacture.Length;

                    this.tbFacture.Text += totalFacture.PadLeft(paddingTotal);

                    this.tbFacture.Text += "\r\n\r\nMerci de votre visite\r\nÀ Bientôt!";

                    DialogResult resultatDialogue = MessageBox.Show("Veuillez prendre votre facture s'il vous plaît...", "Facture", MessageBoxButtons.OK);

                    if (resultatDialogue == DialogResult.OK)
                    {
                        this.tbFacture.Clear();
                    }
                }
            };

            ObservateurFacture observateurFacture = new ObservateurFacture(this.m_facture, ImprimerFacture);
            InitializeComponent();
        }
        private string AlignerGauche(int p_padding, string p_description)
        {
            if (p_description == null)
            {
                throw new ArgumentNullException(nameof(p_description));
            }

            string espaces = "";

            for (int numeroEspace = 0; numeroEspace < p_padding; numeroEspace++)
            {
                espaces += " ";
            }

            return p_description + espaces;
        }
        private string  AlignerDroite(int p_padding, string p_chaine)
        {
            if(p_chaine == null)
            {
                throw new ArgumentNullException(nameof(p_chaine));
            }

            string espaces = "";

            for (int numeroEspace = 0; numeroEspace < p_padding; numeroEspace++)
            {
                espaces += " ";
            }

            return espaces + p_chaine;
        }
    }
}
