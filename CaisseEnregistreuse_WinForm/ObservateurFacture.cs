﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaisseEnregistreuse_WinForm
{
    public class ObservateurFacture : IObserver<FactureEvent>
    {
        private IDisposable m_SeDesabonner;
        private Action<FactureEvent> m_Action;
        public ObservateurFacture(IObservable<FactureEvent> p_sujetObservable, Action<FactureEvent> p_action)
        {
            if(p_sujetObservable == null)
            {
                throw new ArgumentNullException(nameof(p_sujetObservable));
            }
            if (p_action == null)
            {
                throw new ArgumentNullException(nameof(p_action));
            }

            this.m_SeDesabonner = p_sujetObservable.Subscribe(this);
            this.m_Action = p_action;
        }
        public void OnCompleted()
        {
            this.m_SeDesabonner?.Dispose();
            this.m_SeDesabonner = null;
        }

        public void OnError(Exception p_error)
        {
            throw p_error;
        }

        public void OnNext(FactureEvent p_evenement)
        {
            if(p_evenement == null)
            {
                throw new ArgumentNullException(nameof(p_evenement));
            }
            this.m_Action(p_evenement);
        }
    }
}
