﻿
namespace CaisseEnregistreuse_WinForm
{
    partial class fImprimante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbFacture = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbFacture
            // 
            this.tbFacture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFacture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFacture.Location = new System.Drawing.Point(0, 1);
            this.tbFacture.Multiline = true;
            this.tbFacture.Name = "tbFacture";
            this.tbFacture.ReadOnly = true;
            this.tbFacture.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbFacture.Size = new System.Drawing.Size(1109, 1646);
            this.tbFacture.TabIndex = 0;
            this.tbFacture.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fImprimante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(23F, 57F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 1659);
            this.Controls.Add(this.tbFacture);
            this.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(1145, 1762);
            this.MinimumSize = new System.Drawing.Size(1145, 1762);
            this.Name = "fImprimante";
            this.Text = "Imprimante";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFacture;
    }
}