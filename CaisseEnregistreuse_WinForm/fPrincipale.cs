﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CaisseEnregistreuse_WinForm
{
    public partial class fPrincipale : Form
    {
        private Facture m_facture;
        private fClient m_ecranClient;
        private fImprimante m_ticketImprimante;
        public fPrincipale()
        {
            this.m_facture = new Facture();
            InitializeComponent();

            Action<FactureEvent> mettreAJourLignes = evenement =>
            {
                if (evenement.Type.Equals(FactureEventType.AJOUT_LIGNE))
                {
                    string description = evenement.LigneFacture.Description;
                    string quantite = evenement.LigneFacture.Quantite.ToString();
                    string prix = evenement.LigneFacture.PrixUnitaire.ToString() + " $";
                    string total = evenement.LigneFacture.Total.ToString() + " $";

                    this.dgvLignesFacture.Rows.Add(new string[] { description, quantite, prix, total });
                }
                else
                {
                    this.dgvLignesFacture.Rows.Clear();
                }
            };

            Action<FactureEvent> afficherTotal = evenement =>
            {
                this.tbTotal.Text = this.m_facture.Total.ToString() + " $";
                this.tbArticle.Text = "";
                this.nudQuantite.Value = 0;
                this.nudPrix.Value = 0.00m;
            };

            ObservateurFacture observateurLigneFacture = new ObservateurFacture(this.m_facture, mettreAJourLignes);
            ObservateurFacture observateurTotal =  new ObservateurFacture(this.m_facture, afficherTotal);

            this.m_ecranClient = new fClient(this.m_facture);
            this.m_ecranClient.Show();

            this.m_ticketImprimante = new fImprimante(this.m_facture);
            this.m_ticketImprimante.Show();

            this.m_facture.SupprimerLignesFacture();
        }

        private void bSimuler_Click(object sender, EventArgs e)
        {
            LigneFacture ligneFacture = new LigneFacture(this.tbArticle.Text, Convert.ToInt32(nudQuantite.Value), nudPrix.Value);
            this.m_facture.AjouterLigneFacture(ligneFacture);
        }

        private void bPayer_Click(object sender, EventArgs e)
        {
            this.m_facture.SupprimerLignesFacture();
            this.tbTotal.Clear();
        }
    }
}
